export const generateKey = (name) => {
    return `${ name }_${ new Date().getTime() }`;
}
export const getNewFibNums = (fibNums, direction) => {

    const lastIndex = fibNums.length - 1;

    if (direction === 'right') {

        /* сдвигаем последовательность вперед */
        const newFibNum = fibNums[lastIndex - 1] + fibNums[lastIndex];
        return [...fibNums.slice(1), newFibNum];



    }
    if (direction === 'left') {
        /* сдвигаем последовательность назад */
        const newFibNum = fibNums[1] - fibNums[0];
        return [newFibNum, ...fibNums.slice(0, -1)];

    }


}