import { memo } from 'react';
import './style.css';

export const ArrowButton = memo(({ direction, onClick, isHidden }) => {

    const visibility = isHidden ? 'hidden' : 'visible';
 
    const handleClick = () => {
        onClick(direction);
    }

    return <button type='button' className={`arrow-button border ${direction}`} onClick={handleClick} style={{ visibility: visibility }}>
        <div className={`arrow ${direction}`}></div>
    </button>
})
