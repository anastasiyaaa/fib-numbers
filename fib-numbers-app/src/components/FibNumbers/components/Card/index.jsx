import './style.css';
import { getFontSize, generateColorByValue } from './utils';

export const Card = ({ value, id }) => {

    return <div id='animated-card'>
        <div className='card' id={id} style={{
            background: generateColorByValue(value),
            fontSize: `${getFontSize(25, value)}px`
        }}>
            {value}
        </div >  
    </div >
}