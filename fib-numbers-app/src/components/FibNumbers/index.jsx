import { useState } from 'react';
import { ArrowButton } from './components/ArrowButton';
import { Card } from './components/Card';
import './style.css';
import { generateKey, getNewFibNums } from './utils';

export const FibNumbers = () => {

    const [fibNums, setFibNums] = useState([0, 1, 1]);
    const [isBegin, setIsBegin] = useState(true);

    const handleClick = (direction) => {

        const newFibNums = getNewFibNums(fibNums, direction);
        setFibNums(newFibNums);


        if (isBegin && direction === 'right') {
            setIsBegin(false);
        }


        if (direction === 'left' && fibNums[1] - fibNums[0] === 0) {
            setIsBegin(true);
        }


    }

    return <div className='fib-numbers'>


        <ArrowButton direction='left' onClick={handleClick} isHidden={isBegin} /> 

        <div className="fib-numbers-container">
            {fibNums.map((fibNum, index) => {
                return <Card
                    key={generateKey(`card-${index}`)}
                    id={`card-${fibNum}`}
                    value={fibNum}

                />
            })}
        </div>

        <ArrowButton direction='right' onClick={handleClick} isHidden={false} />

    </div>
}