import './App.css'; 
import { FibNumbers } from './components/FibNumbers';

function App() {
  return (
    <div className="App">
      <FibNumbers />
    </div>
  );
}

export default App;
